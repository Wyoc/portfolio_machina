import requests
import json
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
from time import sleep
from subprocess import Popen, PIPE

class Sneaker:
    def __init__(self, title, lowestAsk, highestBid, lastSale):
        self.title = str(title)
        self.lowestAsk = str(lowestAsk)
        self.highestBid = str(highestBid)
        self.lastSale = str(lastSale)

    def toString(self):
        print("Product Title: " + self.title)
        print("Lowest Ask Price = $" + self.lowestAsk)
        print("Highest Ask Price = $" + self.highestBid)
        print("Last Sale Price = $" + self.lastSale)
        print("\n")

def get_info(search):
    search = search.replace(" ", "%20")

    params = {
        "params" : "query=${}&hitsPerPage=1&facets=*".format(search)
    }

    post = requests.post(search_url, json=params, headers=headers)

    resp = json.loads(post.text)

    url_variant = resp['hits'][0]['url']

    api_link = 'https://stockx.com/api/products/' + url_variant + "?includes=market,360"

    get = requests.get(api_link, headers=headers)

    get_json = json.loads(get.text)['Product']
    title =  get_json['title'] #product name

    market = get_json['market']
    lowestAsk = str(market['lowestAsk'])
    highestBid = str(market['highestBid'])
    lastSale = str(market['lastSale'])

    return title, lowestAsk, highestBid, lastSale

def draw_on_image(imagePath, xys, fontSize, texts , rgbs):
    texts = texts[1:]
    img = Image.open(imagePath+'assets.png')
    draw = ImageDraw.Draw(img)
    for xy, t, rgb in zip(xys, texts, rgbs):
        font = ImageFont.truetype("DejaVuSans.ttf", fontSize)
        draw.text((xy[0], xy[1]), '$'+str(t), rgb, font=font)
    img.save(imagePath+'modif.png')


if __name__ == "__main__":
    while True:
        search_url = "https://xw7sbct9v6-dsn.algolia.net/1/indexes/products/query?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%203.26.0&x-algolia-application-id=XW7SBCT9V6&x-algolia-api-key=6bfb5abee4dcd8cea8f0ca1ca085c2b3"

        headers = {
            'Cache-Control' : 'no-cache',
            'Upgrade-Insecure-Requests' : '1',
            'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        }

        searches = ['Nike Blazer Mid Off-White Grim Reaper',
                    'adidas Nizza Donald Glover Off White']

        sneakers = []
        for search in searches:
            title, lowestAsk, highestBid, lastSale = get_info(search)
            sneaker = Sneaker(title, lowestAsk, highestBid, lastSale)
            sneakers.append(sneaker)

        for s in sneakers:
            s.toString()

        rgbs = [(255, 255, 255), 
                (255, 64, 255),
                (0, 250, 146),
                (255, 255, 255),
                (255, 64, 255),
                (0, 250, 146)]

        xys = [[1400, 265],
            [1400, 325],
            [1400, 385],
            [1400, 760],
            [1400, 820],
            [1400, 880]]

        texts = [sneakers[1],lowestAsk,
                sneakers[1].highestBid,
                sneakers[1].lastSale,
                sneakers[0].lowestAsk,
                sneakers[0].highestBid,
                sneakers[0].lastSale]

        print(texts)
        imagePath = ""
        fontSize  = 50

        draw_on_image(imagePath, xys, fontSize, texts, rgbs)

        sleep(600)

